# YBAF Radio Questions

##### AFR eastern departure runway 10L

> *receive ATIS*

* **ATIS IS ALPHA**

> ARCHER GROUND ALPHA FOXTROT ROMEO CESSNA ONE SEVEN TWO EASTERN APRON DUAL
> RECEIVED ALPHA FOR DEPARTURE EAST REQUEST TAXI

* **ALPHA FOXTROT ROMEO TAXIWAY BRAVO CROSS ZERO FOUR RIGHT AND ZERO FOUR LEFT
  HOLDING POINT BRAVO FIVE RUNWAY ONE ZERO LEFT**

> TAXIWAY BRAVO CROSS ZERO FOUR RIGHT AND ZERO FOUR LEFT HOLDING POINT BRAVO
  FIVE RUNWAY ONE ZERO LEFT ALPHA FOXTROT ROMEO

* *taxi to holding point*
* *change to tower frequency*

> ARCHER TOWER ALPHA FOXTROT ROMEO HOLDING POINT BRAVO FIVE RUNWAY ONE ZERO LEFT
  DEPARTING EAST READY

* **ALPHA FOXTROT ROMEO CLEARED FOR TAKE OFF ON RUNWAY ONE ZERO LEFT**

> CLEARED FOR TAKE OFF ON RUNWAY ONE ZERO LEFT ALPHA FOXTROT ROMEO

----

##### AFR eastern departure runway 28R

> *receive ATIS*

* **ATIS IS ALPHA**

> ARCHER GROUND ALPHA FOXTROT ROMEO CESSNA ONE SEVEN TWO EASTERN APRON DUAL
> RECEIVED ALPHA FOR DEPARTURE EAST REQUEST TAXI

* **ALPHA FOXTROT ROMEO TAXI TO HOLDING POINT BRAVO ONE RUNWAY TWO EIGHT RIGHT**

> TAXI TO HOLDING POINT BRAVO ONE RUNWAY TWO EIGHT RIGHT ALPHA FOXTROT ROMEO

* *taxi to holding point*
* *change to tower frequency*

> ARCHER TOWER ALPHA FOXTROT ROMEO HOLDING POINT BRAVO ONE RUNWAY TWO EIGHT
  RIGHT DEPARTING EAST READY

* **ALPHA FOXTROT ROMEO CLEARED FOR TAKE OFF ON RUNWAY TWO EIGHT RIGHT**

> CLEARED FOR TAKE OFF ON RUNWAY TWO EIGHT RIGHT ALPHA FOXTROT ROMEO

----

##### AFR inbound runway 10R

> *receive ATIS*

* **ATIS IS BRAVO**

> ARCHER TOWER ALPHA FOXTROT ROMEO CESSNA ONE SEVEN TWO TARGET MAINTAINING
  ONE THOUSAND FIVE HUNDRED RECEIVED BRAVO INBOUND

* **ALPHA FOXTROT ROMEO JOIN DOWNWIND FOR RUNWAY ONE ZERO RIGHT**

> JOIN DOWNWIND FOR RUNWAY ONE ZERO RIGHT ALPHA FOXTROT ROMEO

----

##### AFR inbound runway 28R

> *receive ATIS*

* **ATIS IS BRAVO**

> ARCHER TOWER ALPHA FOXTROT ROMEO CESSNA ONE SEVEN TWO TARGET MAINTAINING ONE
THOUSAND FIVE HUNDRED RECEIVED BRAVO INBOUND

* **ALPHA FOXTROT ROMEO JOIN FINAL RUNWAY TWO EIGHT RIGHT REPORT TWO MILES**

> JOIN FINAL RUNWAY TWO EIGHT RIGHT ALPHA FOXTROT ROMEO

----

##### AFR after landing

> ARCHER GROUND ALPHA FOXTROT ROMEO REQUEST TAXI TO THE EASTERN APRON

* **ALPHA FOXTROT ROMEO CLEARED FOR TAXI TO THE EASTERN APRON**

> CLEARED FOR TAXI TO THE EASTERN APRON ALPHA FOXTROT ROMEO

----

### Recordings

* [AFR eastern departure runway 10L](https://dl.dropboxusercontent.com/u/7810909/ppl/ybaf-radio-calls/AFR%20depart%20east%2010L.m4a)

* [AFR eastern departure runway 10L (2)](https://dl.dropboxusercontent.com/u/7810909/ppl/ybaf-radio-calls/AFR%20depart%20east%2010L%20(2).m4a)

* [AFR eastern departure runway 28R](https://dl.dropboxusercontent.com/u/7810909/ppl/ybaf-radio-calls/AFR%20depart%20east%2028R.m4a)

* [AFR eastern departure runway 28R (2)](https://dl.dropboxusercontent.com/u/7810909/ppl/ybaf-radio-calls/AFR%20depart%20east%2028R%20(2).m4a)

* [AFR inbound runway 10R](https://dl.dropboxusercontent.com/u/7810909/ppl/ybaf-radio-calls/AFR%20inbound%2010R.m4a)

* [AFR inbound runway 28R](https://dl.dropboxusercontent.com/u/7810909/ppl/ybaf-radio-calls/AFR%20inbound%2028R%20(corrected).m4a)

* [AFR after landing](https://dl.dropboxusercontent.com/u/7810909/ppl/ybaf-radio-calls/AFR%20after%20landing.m4a)

----

#### Practice video

* [AFR eastern departure runway 10L](https://www.youtube.com/watch?v=wMO0Y4BAfho)

* [AFR eastern departure runway 28R](https://www.youtube.com/watch?v=o5zkVvIEyjw)

* [AFR inbound runway 10R](https://www.youtube.com/watch?v=RcY6UfCKfms)

* [AFR inbound runway 28R](https://www.youtube.com/watch?v=jPwVuTIEUEg)

* [AFR after landing](https://www.youtube.com/watch?v=fP3U5kJyFKM)
