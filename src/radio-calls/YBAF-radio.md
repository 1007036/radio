# YBAF Radio

----

### Taxi & Departure Radio

##### ATIS 120.9

* **Terminal Information:** Alpha

* **Runway *(Number L/R)*:** 10L

* **Runway Frequency:** 123.6

* **Wind *(Direction/Speed)*:** 120/08

* **Cloud *(Qty/Height)*:** FEW/2500

* **Temperature:** 25

* **QNH:** 1012

----

* *set barometric pressure according to ATIS QNH.*

##### Taxi Call to Ground 119.9

* **Archer Ground:** archer ground

* **Callsign:** Lima Delta Oscar

* **Type:** Cessna 162

* **Location:** Eastern apron

* **Dual/Solo/PoB:** Dual

* **Received ATIS:** received Alpha

* **Departure *(Direction or Circuits)*:** for departure east

* **Request:** request taxi

----

##### Read Back from Ground (119.9)

* **Taxi way:** taxiway Bravo

* **Specific Instructions:** cross 04R & 04L

* **Holding Point:** holding point Bravo 5

* **Runway for Departure:** Runway 10 Left

* **Call Sign:** Lima Delta Oscar

----

##### Runway Ready

* **Archer Tower (runway frequency):** archer tower

* **Call Sign:** Lima Delta Oscar

* **Ready:** ready

* **Runway:** 10 Left

* **Departing *(Direction or Circuits)*:** departing east

----

### Inbound Radio

##### ATIS 120.9

* **Terminal Information:** Bravo

* **Runway *(Number L/R)*:** 10 Right

* **Runway Frequency:** 118.1

* **Wind *(Direction/Speed)*:** 120/08

* **Cloud *(Qty/Height)*:** Scattered/3000

* **Temperature:** 26

* **QNH:** 1011

##### Inbound Call

* *tune to appropriate tower frequency*

* **Archer Tower:** archer tower

* **Call Sign:** Lima Delta Oscar

* **Type:** Cessna 162

* **Location:** Target

* **Maintaining:** Maintaining 1500

* **Inbound:** inbound

* **Received:** received Bravo

##### Read Back

* **Report:** *report*

* **Specific Point:** Downwind 10 Right

* **Call Sign:** Lima Delta Oscar

##### Report

* **Archer Tower:** archer tower

* **Call Sign:** Lima Delta Oscar

* **Specific Point:** Downwind 10 Right

### Circuit Radio

##### Downwind Call

* **Call Sign:** Lima Delta Oscar

* **Downwind Runway *(L/R)*:** 10 Right

* **Touch'n'Go/Fullstop:** Fullstop

##### Read Back

* **Number in Sequence OR Touch'n'Go/Fullstop:** Cleared full stop

* **Runway *(L/R)*:** runway 10 Right

* **Call Sign:** Lima Delta Oscar

##### After Landing to Ground (119.9)

* **Archer Ground:** archer ground

* **Call Sign:** Lima Delta Oscar

* **Request Taxi for Location:** Request Taxi for Eastern Apron

##### Read Back

* **Cleared Taxi to Specific Point:** cleared taxi to eastern apron

* **Call Sign:** Lima Delta Oscar
